import cardGateway as cg
import tkinter as tk
import view

cards = cg.loadCards(["druid"])

root = tk.Tk()
view.View(root).pack(fill="both", expand=True)
root.mainloop()