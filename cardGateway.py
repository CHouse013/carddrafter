import os

common = 0
uncommon = 1
rare = 2

attack = 0
equipment = 1
item = 2
loot = 3

def loadCards(classes):
    '''
    First index represents rarity, second index represents card type, third index is the card itself
    '''
    cards = [[[] for _ in range(4)] for _ in range(3)]
    #Iterate through selected classes
    for selection in classes:
        #Concatenate the current class to the path
        root = os.getcwd() + "\Cards\\" + selection
        #Walk through selected path
        for path, subdir, files in os.walk(root):
            for name in files:
                #If it's a png, save it to the array
                if name.endswith(".png"):
                    cardPath = os.path.join(path, name)
                    storeCard(cardPath, cards)
    return cards

#Stores the cards in the 3d card array
def storeCard(cardPath, cards):
    rarity = getRarity(cardPath)
    type = getCardType(cardPath)
    if rarity is None or type is None:
        return
    cards[rarity][type].append(cardPath)

def getRarity(cardPath):
    if "Common" in cardPath:
        return common
    if "Uncommon" in cardPath:
        return uncommon
    if "Rare" in cardPath:
        return rare
    print("Card found with no associated rarity, path: " + cardPath)

def getCardType(cardPath):
    if "Attack" in cardPath:
        return attack
    if "Equipment" in cardPath:
        return equipment
    if "Item" in cardPath:
        return item
    if "Loot" in cardPath:
        return loot
    print("Card found with no associated type, path: " + cardPath)

#Returns a list of classes
def getClasses():
    root = os.getcwd() + "\cards\\"
    classes = []
    for file in os.listdir(root):
        classes.append(file)
    return classes