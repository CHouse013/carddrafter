Card drafter application designed to allow you to select certain directories, which are then sub divided into different
rarities, and can pull cards from them at manipulated rates.

Card database must be structured such that they are within the cards directory, then a class directory, then a card type
rarity, so for example, the druid common equipment should be found in the following path

"/Cards/Druid/Common/Equipment"